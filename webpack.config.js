'use strict';

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const ENV = process.env.npm_lifecycle_event;
const isProd = ENV === 'build';

module.exports = (function () {

  let config = {};

  config.entry = {
    app: './src/app.js'
  };


  config.output = {
    path: __dirname + '/dist',

    publicPath: isProd ? './' : '/',

    filename: isProd ? 'js/[name].[hash].js' : 'js/[name].bundle.js'
  };

  if (isProd) {
    config.devtool = 'source-map';
  } else {
    config.devtool = 'eval-source-map';
  }

  // Initialize module
  config.module = {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style-loader', 'css-loader?sourceMap')
    }, {
      test: /\.scss$/,
      loader: 'style!css!sass!resolve-url!sass?sourceMap'
    },
    {
      test: /\.(woff2?|ttf|eot|svg)(.*)?$/,
      loader: "file?name=fonts/[name].[ext]"
    }, {
      test: /\.html$/,
      loader: 'raw'
    }]
  };

  config.plugins = [];

  config.plugins.push(
    new HtmlWebpackPlugin({
      template: './src/public/index.html',
      inject: 'body'
    }),

    new ExtractTextPlugin('style/[name].[hash].css', { disable: !isProd })
  )


  if (isProd) {
    config.plugins.push(
      // Only emit files when there are no errors
      new webpack.NoErrorsPlugin(),

      // Minify all javascript, switch loaders to minimizing mode
      new webpack.optimize.UglifyJsPlugin()
    )
  }

  //Dev server configuration
  config.devServer = {
    contentBase: './src/public',
    stats: 'minimal'
  };

  return config;
})();
