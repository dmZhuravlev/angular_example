export default ($resource) => {
    'ngInject';

    return $resource('http://sarhan-blog.herokuapp.com/api/posts/', null, {
        query: {
            isArray: true,
            transformResponse: function (data) {
                return angular.fromJson(data).responses.posts;
            }
        }
    }); 
}
