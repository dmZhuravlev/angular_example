import PostsService from './posts.service';
import AuthService from './auth.service';
import AddPostServise from './add-post.service';

export default angular.module('app.services', [])
    .factory('PostsService', PostsService)
    .factory('AuthService', AuthService)
    .factory('AddPostServise', AddPostServise)
    .name