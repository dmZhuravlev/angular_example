export default ($resource) => {
    'ngInject';

    return $resource('http://sarhan-blog.herokuapp.com/api/posts/');
}
