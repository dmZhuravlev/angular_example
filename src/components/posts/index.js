import angular from 'angular';
import posts from './posts.component.js';

let appUser = angular.module('app.user', []);
appUser.component('posts', posts);

export default appUser.name;