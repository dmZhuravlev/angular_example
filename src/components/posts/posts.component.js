import template from './posts.html';

class PostCtrl {
    constructor(PostsService, AuthService) {
        'ngInject';
        this.PostsService = PostsService;
        this.init();
    }
    init() {
        this.PostsService.query().$promise.then((posts) => {
            this.posts = posts;
        }, (errResponse) => {
            // fail
        });
    }
}

let posts = {
    template,
    controller: PostCtrl,
    controllerAs: '$ctrl'
}

export default posts;
