import angular from 'angular';

import '../style/app.scss';

import posts from './posts/index.js';
import login from './login/index.js';
import addPost from './add-post/index.js';
import header from './header/index.js';
import template from './main.html';
 
let main = {
    template
}

export default angular.module('app.main', [ posts, login, addPost, header ])
.component('main', main).name;