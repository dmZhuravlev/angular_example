import HeaderComponent from './header.component';
import './header.scss';

export default angular
    .module('app.components.header', [])
    .component('header', HeaderComponent)
    .name;