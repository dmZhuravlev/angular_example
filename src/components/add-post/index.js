import AddPostComponent from './add-post.component';
import './add-post.scss';

export default angular
    .module('app.components.add-post', [])
    .component('addPost', AddPostComponent)
    .name;