import template from './add-post.html';

export default {
    template,
    controller: class AddPost {
        constructor($state, $window, AddPostServise) {
            'ngInject';
            this.$state = $state;
            this.$window = $window;
            this.AddPostServise = AddPostServise;
            //this.$http = $http;
        }

        addPost({ $valid }) {
            if (!$valid) {
                return;
            }
            // this.$http.post('http://sarhan-blog.herokuapp.com/api/posts/', {
            //     "title": this.title,
            //     "description": this.message
            // }).then((response) => {
            //     debugger;
            // });
            this.AddPostServise.save({
                "title": this.title,
                "description": this.message
            }).$promise.then((data) => {
                this.$state.go('main');
            });
        }
    }
};