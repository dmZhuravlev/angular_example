import template from './login.html';

export default {
    template,
    controller: class Login {
        constructor($state, $window, AuthService) {
            'ngInject';
            this.AuthService = AuthService;
            this.$state = $state;
            this.$window = $window;
        }

        submitForm({ $valid }) {
            if ($valid) {
                this.AuthService.save({
                    "name": this.username,
                    "password": this.password
                }).$promise.then((data) => {
                    this.$state.go('main');
                });
            }
        }
    }
};