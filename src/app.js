import angular from 'angular';

import uiRouter from 'angular-ui-router';
import ngResource from 'angular-resource';

import main from './components';
import ServicesModule from './services'

angular.module('app', [ngResource, ServicesModule, uiRouter, main])
.config(($stateProvider, $urlRouterProvider, $httpProvider) => {

    $httpProvider.defaults.withCredentials = true;

    $urlRouterProvider.when('', '/login');
    $urlRouterProvider.when('/', '/login');

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('main', {
            url: '/main',
            template: '<main></main>'
        })
        .state('add-post', {
            url: '/add-post',
            template: '<add-post></add-post>'
        })
        .state('login', {
            url: '/login',
            template: '<login></login>'
        });
});